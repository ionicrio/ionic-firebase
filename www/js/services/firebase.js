angular.module('starter.services')
    .factory("Items",['$firebaseArray',function($firebaseArray) {

        var itemsRef = new Firebase("https://ionicbasic.firebaseio.com/items");
        return $firebaseArray(itemsRef);
}]);