/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers').controller('LoginController',
    ['$scope','OAuth','$cookies','$ionicPopup','$state',
        function ($scope,OAuth,$cookies,$ionicPopup,$state) {

            $scope.user = {
                username:'',
                password:''
            }

            $scope.login = function () {
                OAuth.getAccessToken($scope.user).then(
                    function (data) {
                        window.console.log($cookies.getObject('token'));
                        $state.go('client.view_products');
                    },
                    function(responseError){
                        $ionicPopup.alert({
                            title:'Advertência',
                            template:'Login ou senha inválidos'
                        })
                    })
            }
        }]);
