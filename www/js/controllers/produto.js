/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ProdutoController',
        ['$scope','$state','Items','$ionicLoading',function ($scope,$state,Items,$ionicLoading) {

        $scope.items = Items;

        $scope.saveProduct = function() {

            $ionicLoading.show({
                template:'Carregando...'
            });

            $scope.items.$add({
                "name": this.name,
                "qtd": this.qtd,
                "description": this.description,
                "price": this.price,
            });

            $ionicLoading.hide();
        };

        $scope.pedidos = function () {
            $state.go('client.view_products');
        }

        $scope.voltar = function () {
            $state.go('home');
        }


    }]);
