/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientViewProductController',
        ['$scope','$state','Product','$ionicLoading','$cart'
        ,function($scope,$state,Product,$ionicLoading,$cart){
        $scope.products = [];

        $ionicLoading.show({
            template:'Carregando...'
        });

        Product.query({},function (data) {
            $scope.products = data;
            $ionicLoading.hide();
        },function (dataError) {
            $ionicLoading.hide();
        });

        $scope.voltar = function () {
            $state.go('home');
        }

        $scope.addItem = function (item) {
            item.qtd = 1;
            $cart.addItem(item);
            $state.go('client.checkout')
        }

    }]);
